;;;; limugen.lisp

(in-package #:limugen)

;;; "limugen" goes here. Hacks and glory await!

(defmacro nlet (n letargs &rest body)
  `(labels ((,n ,(mapcar #'car letargs)
	      ,@body))
     (,n ,@(mapcar #'cadr letargs))))

;;extracts all sequences from a given list
(defun get-sequences-from-list (l)
  (nlet  rec ((i l))
	 (cond ((null i) '())
	       (t (append (get-sublists i)
			  (rec (cdr i)))))))

(defun get-sublists (l)
  (nlet rec ((i l))
	(cond ((null i) '())
	      (t (cons i (rec (reverse (cdr (reverse i)))))))))

(defun reduce-and-get-occurrences (original)
  (nlet rec ((l original) (result '()))	
	(cond ((null l) result)
	      ((member (car l) result :test (lambda (a b) (equal a (car b))))   
	       (rec (cdr l) (mapcar (lambda (n)
				      (if (equal (car l) (car n))
					  (list (car n) (1+ (cadr n)))
					  n)) result)))
	      (t (rec (cdr l) (cons (list (car l) 1) result))))))

(defun generate-data-from-list (l)
  ;;couples the above procedures
  (reduce-and-get-occurrences
   (get-sequences-from-list l)))

(defun search-for-continuing-sequences (base data)
  (nlet rec ((l data))
	(cond
	  ((null l) '())
	  ((equal  base
		   (subseq (caar l) 0 (1- (length (caar l)))))
	   (cons (car l) (rec (cdr l))))
	  (t (rec (cdr l))))))
